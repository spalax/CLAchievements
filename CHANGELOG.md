* clachievements 0.1.1 (unreleased)

    * Add python3.6 support.
    * Drop python3.5 support.

    -- Louis Paternault <spalax@gresille.org>

* clachievements 0.1.0 (2016-10-24)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
